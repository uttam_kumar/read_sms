package com.synopi.live.readsms;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    ListView listView;
    ArrayList smsList;
    int i=1;
    public static final int READ_SMS_PERMISSION=100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=findViewById(R.id.listId);

        int permissionCheck=ContextCompat.checkSelfPermission(this,Manifest.permission.READ_SMS);
        if(permissionCheck==PackageManager.PERMISSION_GRANTED){
            showSmsList();
        }
        else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.READ_SMS},READ_SMS_PERMISSION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){

        if(requestCode==READ_SMS_PERMISSION){
            showSmsList();
        }else{
            Toast.makeText(this, "Permission required", Toast.LENGTH_SHORT).show();
        }
    }

    private void showSmsList() {

        Uri dentUri=Uri.parse("content://sms/sent");
        Uri inboxUri=Uri.parse("content://sms/inbox");

        smsList=new ArrayList<>();
        ContentResolver contentResolver=getContentResolver();
        Cursor cursor=contentResolver.query(inboxUri,null,null,null,null);

        while (cursor.moveToNext()){
            i++;
            String number=cursor.getString(cursor.getColumnIndexOrThrow("address"));
            String body=cursor.getString(cursor.getColumnIndexOrThrow("body"));
            String dateS=cursor.getString(cursor.getColumnIndexOrThrow("date"));

            DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");

            Date date = null;
            try {
                date = (Date) formatter.parse(dateS);
                Log.d("TAG", "showSmsList: "+date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Log.d("TAG", "showSmsList: "+date);

            //String ttime=

            smsList.add("Date: "+date+"\nNumber: "+number+"\nBody: "+body);
        }
        Log.d("TAG", "showSmsList: i: "+i);
        cursor.close();

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,smsList);
        listView.setAdapter(adapter);
    }
}
